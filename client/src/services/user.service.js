/* eslint-disable import/no-anonymous-default-export */
import axios from "axios";
import authHeader from "./auth-header";

const API_URL = process.env.REACT_APP_API_URL

const getPublicContent = () => {
  return axios.get(`${API_URL}/home`);
};

const getUserBoard = () => {
  return axios.get(`${API_URL}/user-board`, { headers: authHeader() });
};

const getModeratorBoard = () => {
  return axios.get(`${API_URL}/moderator-board`, { headers: authHeader() });
};

const getAdminBoard = () => {
  return axios.get(`${API_URL}/admin-board`, { headers: authHeader() });
};

export default {
  getPublicContent,
  getUserBoard,
  getModeratorBoard,
  getAdminBoard,
};