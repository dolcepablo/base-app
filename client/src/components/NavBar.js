import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import {
  MDBNavbar,
  MDBNavbarBrand,
  MDBNavbarNav,
  MDBNavItem,
  MDBNavLink,
  MDBNavbarToggler,
  MDBCollapse,
  MDBDropdown,
  MDBDropdownToggle,
  MDBDropdownMenu,
  MDBDropdownItem,
  MDBIcon,
} from "mdbreact";

import { logout } from "../actions/auth";
import { clearMessage } from "../actions/message";

function NavBar() {
  const [showModeratorBoard, setShowModeratorBoard] = useState(false);
  const [showAdminBoard, setShowAdminBoard] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const { user: currentUser } = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  useEffect(() => {
    if (currentUser) {
      setShowModeratorBoard(currentUser.roles.includes("ROLE_MODERATOR"));
      setShowAdminBoard(currentUser.roles.includes("ROLE_ADMIN"));
    }
  }, [currentUser]);

  const logOut = () => {
    dispatch(logout());
  };

  const toggleCollapse = () => {
    setIsOpen(!isOpen);
  };

  return (
    <>
      <MDBNavbar
        color="elegant-color-dark"
        dark
        expand="md"
        className="ml-5 mr-5"
      >
        <MDBNavbarBrand>
          <Link to="/" className="white-text">
            LostArk Skill Calculator
          </Link>
        </MDBNavbarBrand>
        <MDBNavbarToggler onClick={() => toggleCollapse()} />
        <MDBCollapse id="navbarCollapse3" isOpen={isOpen} navbar>
          <MDBNavbarNav left>
            <MDBNavItem>
              <Link to="/home" className="nav-link">
                Home
              </Link>
            </MDBNavItem>

            {showModeratorBoard && (
              <MDBNavItem>
                <Link to="/mod" className="nav-link">
                  Moderator Board
                </Link>
              </MDBNavItem>
            )}

            {showAdminBoard && (
              <MDBNavItem>
                <Link to="/admin" className="nav-link">
                  Admin Board
                </Link>
              </MDBNavItem>
            )}

            {currentUser && (
              <>
                <MDBNavItem>
                  <Link to="/user" className="nav-link">
                    User
                  </Link>
                </MDBNavItem>
                <MDBNavItem>
                  <Link to="/tutorials" className="nav-link">
                    Tutorials
                  </Link>
                </MDBNavItem>
              </>
            )}
          </MDBNavbarNav>

          {currentUser ? (
            <MDBNavbarNav right>
              <MDBNavItem>
                <MDBDropdown>
                  <MDBDropdownToggle nav caret>
                    <MDBIcon icon="user" />
                  </MDBDropdownToggle>
                  <MDBDropdownMenu
                    className="dropdown-default"
                    color="elegant-color"
                  >
                    <MDBDropdownItem>
                      <Link to="/profile" className="nav-link text-dark">
                        Perfil
                      </Link>
                    </MDBDropdownItem>
                    <MDBDropdownItem>
                      <a
                        href="/login"
                        className="nav-link text-dark"
                        onClick={logOut}
                      >
                        Logout
                      </a>
                    </MDBDropdownItem>
                  </MDBDropdownMenu>
                </MDBDropdown>
              </MDBNavItem>
            </MDBNavbarNav>
          ) : (
            <MDBNavbarNav right>
              <MDBNavItem>
                <Link
                  to="/login"
                  className="nav-link"
                  onClick={() => dispatch(clearMessage())}
                >
                  Login
                </Link>
              </MDBNavItem>

              <MDBNavItem>
                <Link
                  to="/register"
                  className="nav-link"
                  onClick={() => dispatch(clearMessage())}
                >
                  Sign Up
                </Link>
              </MDBNavItem>
            </MDBNavbarNav>
          )}
        </MDBCollapse>
      </MDBNavbar>
    </>
  );
}

export default NavBar;
