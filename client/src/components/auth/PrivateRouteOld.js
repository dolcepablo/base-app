import React from "react";
import { useSelector } from "react-redux";
import { Redirect } from "react-router-dom";

const PrivateRoute = (props) => {
  const { user: currentUser } = useSelector((state) => state.auth);

  // if (
  //   (props.privacylevel === "user" && !currentUser) ||
  //   (props.privacylevel === "mod" &&
  //     !currentUser.roles.includes("ROLE_MODERATOR")) ||
  //   (props.privacylevel === "admin" &&
  //     !currentUser.roles.includes("ROLE_ADMIN"))
  // ) {
  //   if (props.privacylevel === "user") {
  //     return <Redirect to="/login" />;
  //   } else if (props.privacylevel === "mod") {
  //     return <Redirect to="/home" />;
  //   } else if (props.privacylevel === "admin") {
  //     return <Redirect to="/login" />;
  //   }
  // }

  if (props.privacylevel === "user" && !currentUser)
    return <Redirect to="/login" />;

  if (
    props.privacylevel === "mod" &&
    currentUser.roles.includes("ROLE_MODERATOR")
  )
    return <Redirect to="/home" />;

  if (
    props.privacylevel === "admin" &&
    currentUser.roles.includes("ROLE_ADMIN")
  )
    return <Redirect to="/home" />;
  return <>{props.children}</>;
};

export default PrivateRoute;
