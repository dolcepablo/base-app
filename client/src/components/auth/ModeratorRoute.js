import React from "react";
import { useSelector } from "react-redux";
import { Route, Redirect } from "react-router-dom";

const ModeratorRoute = ({ component: Component, ...rest }) => {
  const { user: currentUser } = useSelector((state) => state.auth);

  if (!currentUser) return <Redirect to="/login" />;
  return (
    // Show the component only when the user is logged in
    // Otherwise, redirect the user to /signin page
    <Route
      {...rest}
      render={(props) =>
        currentUser.roles.includes("ROLE_MODERATOR") ? (
          <Component {...props} />
        ) : (
          <Redirect to="/home" />
        )
      }
    />
  );
};

export default ModeratorRoute;
