import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Modal, Button } from "react-bootstrap";
import {
  MDBCol,
  MDBRow,
  MDBFormInline,
  MDBBtn,
  MDBModal,
  MDBModalBody,
  MDBModalHeader,
  MDBModalFooter,
} from "mdbreact";
import { Link } from "react-router-dom";
import {
  retrieveTutorials,
  findTutorialsByTitle,
  findPublishedTutorials,
} from "../../actions/tutorials";

const TutorialsList = () => {
  const [currentTutorial, setCurrentTutorial] = useState(null);
  const [currentIndex, setCurrentIndex] = useState(-1);
  const [searchTitle, setSearchTitle] = useState("");
  const [showTutorialModal, setShowTutorialModal] = useState(false);

  const tutorials = useSelector((state) => state.tutorials);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(retrieveTutorials());
  }, []);

  const onChangeSearchTitle = (e) => {
    const searchTitle = e.target.value;
    setSearchTitle(searchTitle);
  };

  const refreshData = () => {
    setCurrentTutorial(null);
    setCurrentIndex(-1);
  };

  const setActiveTutorial = (tutorial, index) => {
    setCurrentTutorial(tutorial);
    setCurrentIndex(index);
    setShowTutorialModal(true);
  };

  const handleShowTutorialModal = () => {
    setShowTutorialModal(!showTutorialModal);
  };

  //   const removeAllTutorials = () => {
  //     dispatch(deleteAllTutorials())
  //       .then(response => {
  //         console.log(response);
  //         refreshData();
  //       })
  //       .catch(e => {
  //         console.log(e);
  //       });
  //   };

  const findByTitle = () => {
    refreshData();
    dispatch(findTutorialsByTitle(searchTitle));
  };

  const findPublished = () => {
    refreshData();
    dispatch(findPublishedTutorials);
  };

  return (
    <>
      <MDBRow className="d-flex justify-content-center">
        <MDBCol md="12" className="d-flex justify-content-center">
          <Link to={"/tutorials/add"} className="btn btn-dark">
            Add new tutorial
          </Link>
        </MDBCol>
        <MDBCol md="12" className="d-flex justify-content-center">
          <MDBFormInline className="md-form">
            <input
              className="form-control"
              type="text"
              placeholder="Search by name"
              value={searchTitle}
              onChange={onChangeSearchTitle}
              aria-label="Search"
            />
            <MDBBtn
              color="unique"
              rounded
              size="sm"
              type="button"
              onClick={findByTitle}
            >
              Search
            </MDBBtn>
          </MDBFormInline>
        </MDBCol>
      </MDBRow>
      <div className="list row">
        <div className="col-md-12">
          <h4>Tutorials List</h4>

          <ul className="list-group">
            {tutorials &&
              tutorials.map((tutorial, index) => (
                <li
                  className={
                    "list-group-item " +
                    (index === currentIndex ? "active" : "")
                  }
                  onClick={() => setActiveTutorial(tutorial, index)}
                  key={index}
                >
                  {tutorial.title}
                </li>
              ))}
          </ul>
        </div>
        <Modal animation={false}>
          ModalHeader
          <Modal.Body>
            {currentTutorial ? (
              <div>
                <h4>Tutorial</h4>
                <div>
                  <label>
                    <strong>Title:</strong>
                  </label>{" "}
                  {currentTutorial.title}
                </div>
                <div>
                  <label>
                    <strong>Description:</strong>
                  </label>{" "}
                  {currentTutorial.description}
                </div>
                <div>
                  <label>
                    <strong>Status:</strong>
                  </label>{" "}
                  {currentTutorial.published ? "Published" : "Pending"}
                </div>

                <Link
                  to={"/tutorials/" + currentTutorial._id}
                  className="badge badge-warning"
                >
                  Edit
                </Link>
              </div>
            ) : (
              <div>
                <br />
                <p>Please click on a Tutorial...</p>
              </div>
            )}
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary">Close</Button>
          </Modal.Footer>
        </Modal>
        <MDBModal
          isOpen={showTutorialModal}
          toggle={() => handleShowTutorialModal()}
          centered
        >
          <MDBModalHeader toggle={() => handleShowTutorialModal()}>
            Tutorial
          </MDBModalHeader>

          <MDBModalBody>
            {currentTutorial ? (
              <div>
                <div>
                  <label>
                    <strong>Title:</strong>
                  </label>{" "}
                  {currentTutorial.title}
                </div>
                <div>
                  <label>
                    <strong>Description:</strong>
                  </label>{" "}
                  {currentTutorial.description}
                </div>
                <div>
                  <label>
                    <strong>Status:</strong>
                  </label>{" "}
                  {currentTutorial.published ? "Published" : "Pending"}
                </div>

                <Link
                  to={"/tutorials/" + currentTutorial._id}
                  className="badge badge-warning"
                >
                  Edit
                </Link>
              </div>
            ) : (
              <div>
                <br />
                <p>Please click on a Tutorial...</p>
              </div>
            )}
          </MDBModalBody>
          <MDBModalFooter>
            <MDBBtn color="secondary" onClick={() => handleShowTutorialModal()}>
              Close
            </MDBBtn>
            <MDBBtn color="primary">Save changes</MDBBtn>
          </MDBModalFooter>
        </MDBModal>
      </div>
    </>
  );
};

export default TutorialsList;
