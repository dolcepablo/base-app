import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  MDBCol,
  MDBRow,
  MDBCard,
  MDBCardBody,
  MDBCardHeader,
  MDBInput,
  MDBTable,
  MDBIcon,
  MDBTooltip,
  MDBTableBody,
  MDBTableHead,
  MDBFormInline,
  MDBBtn,
  MDBModal,
  MDBModalBody,
  MDBModalHeader,
  MDBModalFooter,
} from "mdbreact";
import { Link } from "react-router-dom";
import {
  retrieveTutorials,
  findTutorialsByTitle,
  findPublishedTutorials,
} from "../../actions/tutorials";

const TutorialsList = () => {
  const [currentTutorial, setCurrentTutorial] = useState(null);
  const [currentIndex, setCurrentIndex] = useState(-1);
  const [searchTitle, setSearchTitle] = useState("");
  const [showTutorialModal, setShowTutorialModal] = useState(false);

  const tutorials = useSelector((state) => state.tutorials);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(retrieveTutorials());
  }, []);

  const onChangeSearchTitle = (e) => {
    const searchTitle = e.target.value;
    setSearchTitle(searchTitle);
  };

  const refreshData = () => {
    setCurrentTutorial(null);
    setCurrentIndex(-1);
  };

  const setActiveTutorial = (tutorial, index) => {
    setCurrentTutorial(tutorial);
    setCurrentIndex(index);
    setShowTutorialModal(true);
  };

  const handleShowTutorialModal = () => {
    setShowTutorialModal(!showTutorialModal);
  };

  const findByTitle = () => {
    refreshData();
    dispatch(findTutorialsByTitle(searchTitle));
  };

  const findPublished = () => {
    refreshData();
    dispatch(findPublishedTutorials);
  };

  return (
    <>
      <MDBCard className="elegant-color-transparent">
        <MDBCardHeader
          color="elegant-color-dark"
          className="view view-cascade d-flex justify-content-between align-items-center py-2 mx-0 mb-3"
        >
          <MDBCol
            md="3"
            xs="12"
            className="mt-2 d-flex justify-content-left align-middle text-center"
          >
            <p className="text-left mt-3 mr-auto">Tutorials list</p>
          </MDBCol>
          <MDBCol md="6" xs="12" className="mt-2 d-flex justify-content-center">
            <form onSubmit={findByTitle}>
              <input
                className="form-control"
                type="text"
                placeholder="Search"
                aria-label="Search"
                value={searchTitle}
                onChange={onChangeSearchTitle}
                style={{ backgroundColor: "rgba(193, 193, 193, 0.2)" }}
              />
            </form>
          </MDBCol>
          <MDBCol md="3" xs="12" className="mt-2 d-flex justify-content-center">
            <Link to={"/tutorials/add"} className="px-2 ml-auto">
              <MDBTooltip domElement tag="span" placement="top">
                <span>
                  <MDBBtn
                    rounded
                    size="sm"
                    color="white"
                    className="px-2 ml-auto"
                  >
                    <MDBIcon icon="plus" size="2x" />
                  </MDBBtn>
                </span>
                <span>Add new tutorial</span>
              </MDBTooltip>
            </Link>
          </MDBCol>
        </MDBCardHeader>
        <MDBCardBody>
          <MDBTable className="text-white" btn fixed responsive>
            <thead>
              <tr>
                <th scope="col">Title</th>
              </tr>
            </thead>
            <tbody>
              {tutorials &&
                tutorials.map((tutorial, index) => (
                  <tr>
                    <th
                      onClick={() => setActiveTutorial(tutorial, index)}
                      key={index}
                    >
                      {tutorial.title}
                    </th>
                  </tr>
                ))}
            </tbody>
          </MDBTable>
        </MDBCardBody>
      </MDBCard>

      <MDBModal
        isOpen={showTutorialModal}
        toggle={() => handleShowTutorialModal()}
        centered
      >
        <MDBModalHeader toggle={() => handleShowTutorialModal()}>
          <label>
            <strong>Title:</strong>
          </label>{" "}
          {currentTutorial ? (
            <strong>{currentTutorial.title}</strong>
          ) : (
            <div>
              <br />
              <p>No tutorial selectected...</p>
            </div>
          )}
        </MDBModalHeader>

        <MDBModalBody>
          {currentTutorial ? (
            <div>
              <div>
                <label>
                  <strong>Description:</strong>
                </label>{" "}
                {currentTutorial.description}
              </div>
              <div>
                <label>
                  <strong>Status:</strong>
                </label>{" "}
                {currentTutorial.published ? "Published" : "Pending"}
              </div>
            </div>
          ) : (
            <div>
              <br />
              <p>Please click on a Tutorial...</p>
            </div>
          )}
        </MDBModalBody>
        <MDBModalFooter className="d-flex justify-content-center">
          {currentTutorial ? (
            <Link to={"/tutorials/" + currentTutorial._id}>
              <MDBTooltip domElement tag="span" placement="top">
                <span>
                  <MDBBtn
                    rounded
                    size="sm"
                    gradient="blue"
                    className="px-2 ml-auto"
                  >
                    <MDBIcon className="px-1" icon="edit" size="2x" />
                  </MDBBtn>
                </span>
                <span>Edit tutorial</span>
              </MDBTooltip>
            </Link>
          ) : (
            <div>
              <br />
              <p>Nothing to do here</p>
            </div>
          )}
        </MDBModalFooter>
      </MDBModal>
    </>
  );
};

export default TutorialsList;
