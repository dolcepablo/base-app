import React from "react";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import { MDBContainer, MDBCol } from "mdbreact";

import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";
import "./App.css";

import Login from "./components/auth/Login";
import Register from "./components/auth/Register";
import Home from "./components/Home";
import NavBar from "./components/NavBar";
import Profile from "./components/auth/Profile";
import BoardUser from "./components/user/BoardUser";
import BoardModerator from "./components/moderator/BoardModerator";
import BoardAdmin from "./components/admin/BoardAdmin";

import EditTutorial from "./components/tutorial/EditTutorial";
import AddTutorial from "./components/tutorial/AddTutorial";
import TutorialList from "./components/tutorial/TutorialList";

import PrivateRoute from "./components/auth/PrivateRoute";
import AdminRoute from "./components/auth/AdminRoute";
import ModeratorRoute from "./components/auth/ModeratorRoute";

const App = () => {
  return (
    <BrowserRouter>
      <MDBCol md="11" style={{ float: "none", margin: "auto" }}>
        <NavBar></NavBar>
      </MDBCol>

      <MDBContainer className="mt-3">
        <Switch>
          <Route path="/login" component={Login} />
          <Route path="/register" component={Register} />
          <Route exact path="/tutorials" component={TutorialList} />
          <Route path="/tutorials/add" component={AddTutorial} />
          <Route path="/tutorials/:id" component={EditTutorial} />
          <Route exact path={["/", "/home"]} component={Home} />
          <ModeratorRoute path="/mod" component={BoardModerator} />
          <AdminRoute path="/admin" component={BoardAdmin} />
          <PrivateRoute path="/user" component={BoardUser} />
          <Route path="/profile" component={Profile} />
        </Switch>
      </MDBContainer>
    </BrowserRouter>
  );
};

export default App;
