module.exports = app => {
    const builds = require("../controllers/build.controller.js");
  
    var router = require("express").Router();
  
    // Create a new Tutorial
    router.post("/", builds.create);
  
    // Retrieve all Tutorials
    router.get("/", builds.findAll);
    
    // Retrieve a single Tutorial with id
    router.get("/:id", builds.findOne);
  
    // Update a Tutorial with id
    router.put("/:id", builds.update);
  
    // Delete a Tutorial with id
    router.delete("/:id", builds.delete);
  
    // Create a new Tutorial
    router.delete("/", builds.deleteAll);
  
    app.use('/api/builds', router);
  };