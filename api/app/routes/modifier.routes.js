module.exports = app => {
    const modifiers = require("../controllers/modifier.controller.js");
  
    var router = require("express").Router();
  
    // Create a new Tutorial
    router.post("/", modifiers.create);
  
    // Retrieve all Tutorials
    router.get("/", modifiers.findAll);
    
    // Retrieve a single Tutorial with id
    router.get("/:id", modifiers.findOne);
  
    // Update a Tutorial with id
    router.put("/:id", modifiers.update);
  
    // Delete a Tutorial with id
    router.delete("/:id", modifiers.delete);
  
    // Create a new Tutorial
    router.delete("/", modifiers.deleteAll);
  
    app.use('/api/modifiers', router);
  };