module.exports = app => {
    const skills = require("../controllers/skill.controller.js");
  
    var router = require("express").Router();
  
    // Create a new Tutorial
    router.post("/", skills.create);
  
    // Retrieve all Tutorials
    router.get("/", skills.findAll);
    
    // Retrieve a single Tutorial with id
    router.get("/:id", skills.findOne);
  
    // Update a Tutorial with id
    router.put("/:id", skills.update);
  
    // Delete a Tutorial with id
    router.delete("/:id", skills.delete);
  
    // Create a new Tutorial
    router.delete("/", skills.deleteAll);
  
    app.use('/api/skills', router);
  };