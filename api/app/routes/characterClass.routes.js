module.exports = app => {
    const characterClasses = require("../controllers/characterClass.controller.js");
  
    var router = require("express").Router();
  
    // Create a new Tutorial
    router.post("/", characterClasses.create);
  
    // Retrieve all Tutorials
    router.get("/", characterClasses.findAll);
    
    // Retrieve a single Tutorial with id
    router.get("/:id", characterClasses.findOne);
  
    // Update a Tutorial with id
    router.put("/:id", characterClasses.update);
  
    // Delete a Tutorial with id
    router.delete("/:id", characterClasses.delete);
  
    // Create a new Tutorial
    router.delete("/", characterClasses.deleteAll);
  
    app.use('/api/characterClasses', router);
  };