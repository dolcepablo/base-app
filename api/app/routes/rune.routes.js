module.exports = app => {
    const runes = require("../controllers/rune.controller.js");
  
    var router = require("express").Router();
  
    // Create a new Tutorial
    router.post("/", runes.create);
  
    // Retrieve all Tutorials
    router.get("/", runes.findAll);
    
    // Retrieve a single Tutorial with id
    router.get("/:id", runes.findOne);
  
    // Update a Tutorial with id
    router.put("/:id", runes.update);
  
    // Delete a Tutorial with id
    router.delete("/:id", runes.delete);
  
    // Create a new Tutorial
    router.delete("/", runes.deleteAll);
  
    app.use('/api/runes', router);
  };