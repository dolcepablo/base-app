module.exports = app => {
    const fullSkills = require("../controllers/fullSkill.controller.js");
  
    var router = require("express").Router();
  
    // Create a new Tutorial
    router.post("/", fullSkills.create);
  
    // Retrieve all Tutorials
    router.get("/", fullSkills.findAll);
    
    // Retrieve a single Tutorial with id
    router.get("/:id", fullSkills.findOne);
  
    // Update a Tutorial with id
    router.put("/:id", fullSkills.update);
  
    // Delete a Tutorial with id
    router.delete("/:id", fullSkills.delete);
  
    // Create a new Tutorial
    router.delete("/", fullSkills.deleteAll);
  
    app.use('/api/fullSkills', router);
  };