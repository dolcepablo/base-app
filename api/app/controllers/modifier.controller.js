const db = require("../models");
const {Modifier} = db.modifier

// Create and Save a new Modifier
exports.create = async(req, res) => {
  // Validate request
  if (!req.body.name) return res.status(400).send({ message: "Content can not be empty!" })

  const { skill, tier, name, description, icon, cost } = req.body

    //creamos un documento en base a esas propiedades
    let modifier = new Modifier({
        skill, tier, name, description, icon, cost
    })

    //guardamos el documento nuevo en la base de datos, mandamos una respuesta al cliente, y manejamos el error si es que se da
    try {
        await modifier.save()
        res.send(modifier)
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }



};

// Retrieve all Modifiers from the database.
exports.findAll = async(req, res) => {
    const name = req.query.name
    let condition = name ? { name: { $regex: new RegExp(name), $options: "i" } } : {}

    try {
        const modifiers = await Modifier.find(condition)
        res.send(modifiers)
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }
};

// Find a single Modifier with an id
exports.findOne = async(req, res) => {
    const id = req.params.id

    try {
        const modifier = await Modifier.findById(id)
        if(!modifier) res.status(404).send({message: "Not found Modifier with id " + id})
        else res.send(modifier)
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }
};

// Update a Modifier by the id in the request
exports.update = async(req, res) => {
    if (!req.body) return res.status(400).send({message: 'Data to update can not be empty!'})

    const id = req.params.id

    try {
        const modifier = await Modifier.findById(id)
        if(!modifier) return res.status(404).send("Todo not found...");
        const updatedModifier = await Modifier.findByIdAndUpdate(id, req.body, { useFindAndModify: false, new: true})
        res.send("Modifier updated successfully: " + updatedModifier)
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }
};

// Delete a Modifier with the specified id in the request
exports.delete = async(req, res) => {
    const id = req.params.id
    try {
        const modifier = await Modifier.findById(id)
        if(!modifier) return res.status(404).send("Modifier not found...");
        
        const deletedModifier = await Modifier.findByIdAndDelete(id)
        res.send("Modifier deleted successfully:" + deletedModifier)
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }
};

// Delete all Modifiers from the database.
exports.deleteAll = async(req, res) => {
    try {
        let modifiers = await Modifier.deleteMany()
        res.send({message: `${modifiers.deletedCount} modifiers were deleted successfully.`})
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }
};
