const db = require("../models");
const {Skill} = db.skill

// Create and Save a new Skill
exports.create = async(req, res) => {
  // Validate request
  if (!req.body.name) return res.status(400).send({ message: "Content can not be empty!" })

  const { name, description, icon, cooldown, mana, effect, characterClass } = req.body

    //creamos un documento en base a esas propiedades
    let skill = new Skill({
        name, description, icon, cooldown, mana, effect, characterClass
    })

    //guardamos el documento nuevo en la base de datos, mandamos una respuesta al cliente, y manejamos el error si es que se da
    try {
        await skill.save()
        res.send(skill)
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }



};

// Retrieve all Skills from the database.
exports.findAll = async(req, res) => {
    const name = req.query.name
    let condition = name ? { name: { $regex: new RegExp(name), $options: "i" } } : {}

    try {
        const skills = await Skill.find(condition)
        res.send(skills)
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }
};

// Find a single Skill with an id
exports.findOne = async(req, res) => {
    const id = req.params.id

    try {
        const skill = await Skill.findById(id)
        if(!skill) res.status(404).send({message: "Not found Skill with id " + id})
        else res.send(skill)
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }
};

// Update a Skill by the id in the request
exports.update = async(req, res) => {
    if (!req.body) return res.status(400).send({message: 'Data to update can not be empty!'})

    const id = req.params.id

    try {
        const skill = await Skill.findById(id)
        if(!skill) return res.status(404).send("Todo not found...");
        const updatedSkill = await Skill.findByIdAndUpdate(id, req.body, { useFindAndModify: false, new: true})
        res.send("Skill updated successfully: " + updatedSkill)
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }
};

// Delete a Skill with the specified id in the request
exports.delete = async(req, res) => {
    const id = req.params.id
    try {
        const skill = await Skill.findById(id)
        if(!skill) return res.status(404).send("Skill not found...");
        
        const deletedSkill = await Skill.findByIdAndDelete(id)
        res.send("Skill deleted successfully:" + deletedSkill)
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }
};

// Delete all Skills from the database.
exports.deleteAll = async(req, res) => {
    try {
        let skills = await Skill.deleteMany()
        res.send({message: `${skills.deletedCount} skills were deleted successfully.`})
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }
};
