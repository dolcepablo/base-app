const db = require("../models");
const {CharacterClass} = db.characterClass

// Create and Save a new CharacterClass
exports.create = async(req, res) => {
  // Validate request
  if (!req.body.name) return res.status(400).send({ message: "Content can not be empty!" })

  const { name } = req.body

    //creamos un documento en base a esas propiedades
    let characterClass = new CharacterClass({
        name
    })

    //guardamos el documento nuevo en la base de datos, mandamos una respuesta al cliente, y manejamos el error si es que se da
    try {
        await characterClass.save()
        res.send(characterClass)
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }



};

// Retrieve all CharacterClasses from the database.
exports.findAll = async(req, res) => {
    const name = req.query.name
    let condition = name ? { name: { $regex: new RegExp(name), $options: "i" } } : {}

    try {
        const characterClasses = await CharacterClass.find(condition)
        res.send(characterClasses)
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }
};

// Find a single CharacterClass with an id
exports.findOne = async(req, res) => {
    const id = req.params.id

    try {
        const characterClass = await CharacterClass.findById(id)
        if(!characterClass) res.status(404).send({message: "Not found CharacterClass with id " + id})
        else res.send(characterClass)
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }
};

// Update a CharacterClass by the id in the request
exports.update = async(req, res) => {
    if (!req.body) return res.status(400).send({message: 'Data to update can not be empty!'})

    const id = req.params.id

    try {
        const characterClass = await CharacterClass.findById(id)
        if(!characterClass) return res.status(404).send("Todo not found...");
        const updatedCharacterClass = await CharacterClass.findByIdAndUpdate(id, req.body, { useFindAndModify: false, new: true})
        res.send("CharacterClass updated successfully: " + updatedCharacterClass)
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }
};

// Delete a CharacterClass with the specified id in the request
exports.delete = async(req, res) => {
    const id = req.params.id
    try {
        const characterClass = await CharacterClass.findById(id)
        if(!characterClass) return res.status(404).send("CharacterClass not found...");
        
        const deletedCharacterClass = await CharacterClass.findByIdAndDelete(id)
        res.send("CharacterClass deleted successfully:" + deletedCharacterClass)
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }
};

// Delete all CharacterClasses from the database.
exports.deleteAll = async(req, res) => {
    try {
        let characterClasses = await CharacterClass.deleteMany()
        res.send({message: `${characterClasses.deletedCount} characterClasses were deleted successfully.`})
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }
};
