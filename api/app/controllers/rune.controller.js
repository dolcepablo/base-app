const db = require("../models");
const {Rune} = db.rune

// Create and Save a new Rune
exports.create = async(req, res) => {
  // Validate request
  if (!req.body.name) return res.status(400).send({ message: "Content can not be empty!" })

  const { name, description, icon, grade } = req.body

    //creamos un documento en base a esas propiedades
    let rune = new Rune({
        name, description, icon, grade
    })

    //guardamos el documento nuevo en la base de datos, mandamos una respuesta al cliente, y manejamos el error si es que se da
    try {
        await rune.save()
        res.send(rune)
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }



};

// Retrieve all Runes from the database.
exports.findAll = async(req, res) => {
    const name = req.query.name
    let condition = name ? { name: { $regex: new RegExp(name), $options: "i" } } : {}

    try {
        const runes = await Rune.find(condition)
        res.send(runes)
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }
};

// Find a single Rune with an id
exports.findOne = async(req, res) => {
    const id = req.params.id

    try {
        const rune = await Rune.findById(id)
        if(!rune) res.status(404).send({message: "Not found Rune with id " + id})
        else res.send(rune)
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }
};

// Update a Rune by the id in the request
exports.update = async(req, res) => {
    if (!req.body) return res.status(400).send({message: 'Data to update can not be empty!'})

    const id = req.params.id

    try {
        const rune = await Rune.findById(id)
        if(!rune) return res.status(404).send("Todo not found...");
        const updatedRune = await Rune.findByIdAndUpdate(id, req.body, { useFindAndModify: false, new: true})
        res.send("Rune updated successfully: " + updatedRune)
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }
};

// Delete a Rune with the specified id in the request
exports.delete = async(req, res) => {
    const id = req.params.id
    try {
        const rune = await Rune.findById(id)
        if(!rune) return res.status(404).send("Rune not found...");
        
        const deletedRune = await Rune.findByIdAndDelete(id)
        res.send("Rune deleted successfully:" + deletedRune)
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }
};

// Delete all Runes from the database.
exports.deleteAll = async(req, res) => {
    try {
        let runes = await Rune.deleteMany()
        res.send({message: `${runes.deletedCount} runes were deleted successfully.`})
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }
};
