const db = require("../models");
const {Tutorial} = db.tutorial

// Create and Save a new Tutorial
exports.create = async(req, res) => {
  // Validate request
  if (!req.body.title) return res.status(400).send({ message: "Content can not be empty!" })

  const { title, description, published} = req.body

    //creamos un documento en base a esas propiedades
    let tutorial = new Tutorial({
        title, description, published
    })

    //guardamos el documento nuevo en la base de datos, mandamos una respuesta al cliente, y manejamos el error si es que se da
    try {
        await tutorial.save()
        res.send(tutorial)
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }



};

// Retrieve all Tutorials from the database.
exports.findAll = async(req, res) => {
    const title = req.query.title
    let condition = title ? { title: { $regex: new RegExp(title), $options: "i" } } : {}

    try {
        const tutorials = await Tutorial.find(condition)
        res.send(tutorials)
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }
};

// Find a single Tutorial with an id
exports.findOne = async(req, res) => {
    const id = req.params.id

    try {
        const tutorial = await Tutorial.findById(id)
        if(!tutorial) res.status(404).send({message: "Not found Tutorial with id " + id})
        else res.send(tutorial)
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }
};

// Update a Tutorial by the id in the request
exports.update = async(req, res) => {
    if (!req.body) return res.status(400).send({message: 'Data to update can not be empty!'})

    const id = req.params.id

    try {
        const tutorial = await Tutorial.findById(id)
        if(!tutorial) return res.status(404).send("Todo not found...");
        const updatedTutorial = await Tutorial.findByIdAndUpdate(id, req.body, { useFindAndModify: false, new: true})
        res.send("Tutorial updated successfully: " + updatedTutorial)
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }
};

// Delete a Tutorial with the specified id in the request
exports.delete = async(req, res) => {
    const id = req.params.id
    try {
        const tutorial = await Tutorial.findById(id)
        if(!tutorial) return res.status(404).send("Tutorial not found...");
        
        const deletedTutorial = await Tutorial.findByIdAndDelete(id)
        res.send("Tutorial deleted successfully:" + deletedTutorial)
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }
};

// Delete all Tutorials from the database.
exports.deleteAll = async(req, res) => {
    try {
        let tutorials = await Tutorial.deleteMany()
        res.send({message: `${tutorials.deletedCount} tutorials were deleted successfully.`})
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }
};

// Find all published Tutorials
exports.findAllPublished = async(req, res) => {
    try {
        let tutorials = await Tutorial.find({published: true})
        res.send(tutorials)
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }
};