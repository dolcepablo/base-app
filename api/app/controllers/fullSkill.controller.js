const db = require("../models");
const {FullSkill} = db.fullSkill

// Create and Save a new FullSkill
exports.create = async(req, res) => {
  // Validate request
  if (!req.body.skill) return res.status(400).send({ message: "Content can not be empty!" })

  const { skill, modifiers, rune } = req.body

    //creamos un documento en base a esas propiedades
    let fullSkill = new FullSkill({
        skill, modifiers, rune
    })

    //guardamos el documento nuevo en la base de datos, mandamos una respuesta al cliente, y manejamos el error si es que se da
    try {
        await fullSkill.save()
        res.send(fullSkill)
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }



};

// Retrieve all FullFullSkills from the database.
exports.findAll = async(req, res) => {
    const skill = req.query.skill
    let condition = skill ? { name: { $regex: new RegExp(skill), $options: "i" } } : {}

    try {
        const fullSkills = await FullSkill.find(condition)
        res.send(fullSkills)
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }
};

// Find a single FullSkill with an id
exports.findOne = async(req, res) => {
    const id = req.params.id

    try {
        const fullSkill = await FullSkill.findById(id)
        if(!fullSkill) res.status(404).send({message: "Not found FullSkill with id " + id})
        else res.send(fullSkill)
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }
};

// Update a FullSkill by the id in the request
exports.update = async(req, res) => {
    if (!req.body) return res.status(400).send({message: 'Data to update can not be empty!'})

    const id = req.params.id

    try {
        const fullSkill = await FullSkill.findById(id)
        if(!fullSkill) return res.status(404).send("Todo not found...");
        const updatedFullSkill = await FullSkill.findByIdAndUpdate(id, req.body, { useFindAndModify: false, new: true})
        res.send("FullSkill updated successfully: " + updatedFullSkill)
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }
};

// Delete a FullSkill with the specified id in the request
exports.delete = async(req, res) => {
    const id = req.params.id
    try {
        const fullSkill = await FullSkill.findById(id)
        if(!fullSkill) return res.status(404).send("FullSkill not found...");
        
        const deletedFullSkill = await FullSkill.findByIdAndDelete(id)
        res.send("FullSkill deleted successfully:" + deletedFullSkill)
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }
};

// Delete all FullFullSkills from the database.
exports.deleteAll = async(req, res) => {
    try {
        let fullSkills = await FullSkill.deleteMany()
        res.send({message: `${fullSkills.deletedCount} fullSkills were deleted successfully.`})
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }
};
