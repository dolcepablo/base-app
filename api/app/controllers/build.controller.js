const db = require("../models");
const {Build} = db.build

// Create and Save a new Build
exports.create = async(req, res) => {
  // Validate request
  if (!req.body.name) return res.status(400).send({ message: "Content can not be empty!" })

  const { name, description, author, fullSkills, likes } = req.body

    //creamos un documento en base a esas propiedades
    let build = new Build({
        name, description, author, fullSkills, likes
    })

    //guardamos el documento nuevo en la base de datos, mandamos una respuesta al cliente, y manejamos el error si es que se da
    try {
        await build.save()
        res.send(build)
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }



};

// Retrieve all Builds from the database.
exports.findAll = async(req, res) => {
    const name = req.query.name
    let condition = name ? { name: { $regex: new RegExp(name), $options: "i" } } : {}

    try {
        const builds = await Build.find(condition)
        res.send(builds)
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }
};

// Find a single Build with an id
exports.findOne = async(req, res) => {
    const id = req.params.id

    try {
        const build = await Build.findById(id)
        if(!build) res.status(404).send({message: "Not found Build with id " + id})
        else res.send(build)
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }
};

// Update a Build by the id in the request
exports.update = async(req, res) => {
    if (!req.body) return res.status(400).send({message: 'Data to update can not be empty!'})

    const id = req.params.id

    try {
        const build = await Build.findById(id)
        if(!build) return res.status(404).send("Todo not found...");
        const updatedBuild = await Build.findByIdAndUpdate(id, req.body, { useFindAndModify: false, new: true})
        res.send("Build updated successfully: " + updatedBuild)
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }
};

// Delete a Build with the specified id in the request
exports.delete = async(req, res) => {
    const id = req.params.id
    try {
        const build = await Build.findById(id)
        if(!build) return res.status(404).send("Build not found...");
        
        const deletedBuild = await Build.findByIdAndDelete(id)
        res.send("Build deleted successfully:" + deletedBuild)
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }
};

// Delete all Builds from the database.
exports.deleteAll = async(req, res) => {
    try {
        let builds = await Build.deleteMany()
        res.send({message: `${builds.deletedCount} builds were deleted successfully.`})
    } catch (error) {
        res.status(500).send(error.message)
        console.log(error.message)
    }
};
