const mongoose = require("mongoose")

const fullSkillSchema = new mongoose.Schema({
    skill: { type: mongoose.Schema.Types.ObjectId, ref: "Skill", required: true },
    modifiers: [{ type: mongoose.Schema.Types.ObjectId, ref: "Modifier" }],
    rune: { type: mongoose.Schema.Types.ObjectId, ref: "Rune" }
})

const FullSkill = mongoose.model('FullSkill', fullSkillSchema)

exports.FullSkill = FullSkill