const mongoose = require("mongoose")

const runeSchema = new mongoose.Schema({
    name: { type: String, required: true, minlength: 3, maxlenght: 200 },
    description: { type: String, minlength: 3},
    icon: { type: String },
    grade: { type: String }
})

const Rune = mongoose.model('Rune', runeSchema)

exports.Rune = Rune