const mongoose = require("mongoose")

const buildSchema = new mongoose.Schema({
    name: { type: String, required: true, minlength: 3, maxlength: 200 },
    description: { type: String, minlength: 3, maxlength: 1000},
    author: { type: mongoose.Schema.Types.ObjectId, ref: "User"},
    fullSkills: [{ type: mongoose.Schema.Types.ObjectId, ref: "FullSkill" }],
    likes: [{ type: mongoose.Schema.Types.ObjectId, ref: "User" }]
})

const Build = mongoose.model('Build', buildSchema)

exports.Build = Build