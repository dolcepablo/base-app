const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const db = {};

db.mongoose = mongoose;

db.user = require("./user.model");
db.role = require("./role.model");
db.tutorial = require("./tutorial.model")
db.characterClass = require("./characterClass.model")
db.skill = require("./skill.model")
db.modifier = require("./modifier.model")
db.rune = require("./rune.model")
db.fullSkill = require("./fullSkill.model")
db.build = require("./build.model")

db.ROLES = ["user", "admin", "moderator"];

module.exports = db;