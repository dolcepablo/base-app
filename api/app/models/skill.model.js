const mongoose = require("mongoose")

const skillSchema = new mongoose.Schema({
    name: { type: String, required: true, minlength: 3, maxlenght: 200 },
    description: { type: String, minlength: 3},
    icon: { type: String },
    cooldown: { type: Number },
    mana: { type: Number },
    effect: { type: String },
    characterClass: { type: mongoose.Schema.Types.ObjectId, ref: "characterClass" }
})

const Skill = mongoose.model('Skill', skillSchema)

exports.Skill = Skill