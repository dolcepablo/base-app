const mongoose = require("mongoose")

const modifierSchema = new mongoose.Schema({
    skill: { type: mongoose.Schema.Types.ObjectId, ref: "Skill", required: true},
    tier: { type: Number, required: true },
    name: { type: String, required: true, minlength: 3, maxlenght: 200 },
    description: { type: String, minlength: 3},
    icon: { type: String },
    cost: { type: Number }
})

const Modifier = mongoose.model('Modifier', modifierSchema)

exports.Modifier = Modifier