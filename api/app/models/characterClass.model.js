const mongoose = require("mongoose")

const characterClassSchema = new mongoose.Schema({
    name: { type: String, required: true, minlength: 3, maxlenght: 200 }

})

const CharacterClass = mongoose.model('CharacterClass', characterClassSchema)

exports.CharacterClass = CharacterClass