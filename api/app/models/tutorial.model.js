const mongoose = require("mongoose")

const tutorialSchema = new mongoose.Schema({
    title: { type: String, required: true, minlength: 3, maxlenght: 200 },
    description: { type: String, minlength: 3, maxlenght: 30 },
    published: { type: Boolean, default: false}

}, { timestamps: true })

const Tutorial = mongoose.model('Tutorial', tutorialSchema)

exports.Tutorial = Tutorial